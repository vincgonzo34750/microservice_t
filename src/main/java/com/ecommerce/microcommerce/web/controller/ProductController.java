package com.ecommerce.microcommerce.web.controller;

import com.ecommerce.microcommerce.model.Product;
import com.ecommerce.microcommerce.web.dao.ProductDao;
import com.ecommerce.microcommerce.web.exceptions.ProductNotFoundException;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Objects;

@Api("API pour les opérations CRUD sur les produits.")
@RestController
public class ProductController {
    private final ProductDao productDao;

    public ProductController(ProductDao productDao){
        this.productDao = productDao;
    }

    @GetMapping("/products")
    public MappingJacksonValue productList(){
        List<Product> products = this.productDao.findAll();
        SimpleBeanPropertyFilter myfilter = SimpleBeanPropertyFilter.serializeAllExcept("buyPrice");
        FilterProvider listFilters = new SimpleFilterProvider().addFilter("myDynamicFilter", myfilter);
        MappingJacksonValue productsFilter = new MappingJacksonValue(products);
        productsFilter.setFilters(listFilters);
        return productsFilter;
    }

    @ApiOperation("Récupère un produit grâce à son ID à condition que celui-ci soit en stock!")
    @GetMapping("/product/{id}")
    public Product displayProduct(@PathVariable int id) {
        Product p = this.productDao.findById(id);
        if(p == null) throw new ProductNotFoundException("Product with id :" + id + " doesn't exist.");
        return p;
    }

    @GetMapping(value = "/products/above/{prixLimit}")
    public List<Product> testeDeRequetes(@PathVariable int prixLimit)
    {
        return productDao.findByPriceGreaterThan(prixLimit);
    }

    @PostMapping("/products")
    public ResponseEntity<Product> addProduct(@Valid @RequestBody Product product){
        Product productAdded = productDao.save(product);
        if (Objects.isNull(productAdded)) {
            return ResponseEntity.noContent().build();
        }
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/product/{id}")
    public void deleteProduct(@PathVariable int id){
        productDao.deleteById(id);
    }

    @PutMapping ("/product")
    public void updateProduct(@RequestBody Product product)
    {
        productDao.save(product);
    }
}
